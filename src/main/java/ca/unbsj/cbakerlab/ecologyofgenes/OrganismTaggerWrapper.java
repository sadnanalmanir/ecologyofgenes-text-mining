/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ca.unbsj.cbakerlab.ecologyofgenes;

/**
 *
 * @author sadnana
 */


import gate.AnnotationSet;
import gate.CorpusController;
import gate.Factory;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;



public class OrganismTaggerWrapper {
	private static final Logger log = Logger.getLogger(OrganismTaggerWrapper.class);

	
	private static CorpusController controller;


	
	static {
		try {
			//Gate.getCreoleRegister().registerDirectories(new File(Gate.getPluginsHome(), "JAPE_Plus").toURI().toURL());

			//controller = (CorpusController) PersistenceManager.loadObjectFromFile(new File("/home/artjomk/bin/GATE-7.0/OrganismTagger/gate/OrganismTagger.xgapp"));
			////!!!controller = (CorpusController) PersistenceManager.loadObjectFromFile(new File("/home/artjomk/workspace2/EcotoxicologyTextMining/OrganismTagger/application.xgapp"));
			//	controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///ssd/home/artjomk/bin/GATE-7.1/OrganismTagger/gate/OrganismTagger.xgapp"));
			
			
			//controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///home/artjomk/bin/GATE-7.1/plugins/OrganismTagger/application.xgapp"));
			
			///!!! this worked controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///home/artjomk/bin/GATE-7.1/plugins/OrganismTagger/applicationNoStrainRecognition.xgapp"));
			
			
			//controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file://"+Gate.getPluginsHome()+"/OrganismTagger/application.xgapp"));
			controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file://"+Gate.getPluginsHome()+"/OrganismTagger/applicationNoStrainRecognition.xgapp"));

			
			///controller = (CorpusController) PersistenceManager.loadObjectFromUrl((log.getClass().getResource("/gate/plugins/application1/application.xgapp").toURI().toURL()));
		} catch (PersistenceException e1) {
			e1.printStackTrace();
		} catch (ResourceInstantiationException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		//} catch (URISyntaxException e) {
		//	e.printStackTrace();
		} catch (GateException e) {
			e.printStackTrace();
		}
	}
	
	
	private static gate.Document process(String text) throws ResourceInstantiationException, ExecutionException{
		gate.Document doc = Factory.newDocument(text);    	
		gate.Corpus gateCorpus = Factory.newCorpus("corpus"+doc.getName());    			
        gateCorpus.add(doc);
        synchronized(controller){
        	controller.setCorpus(gateCorpus);   
        	controller.execute();		
        	Factory.deleteResource(gateCorpus);	
        }
		return doc;
	}
	
	
	public static void annotate(gate.Document doc) throws ResourceInstantiationException, ExecutionException {		
			String text = doc.getContent().toString();		
			gate.Document annotatedDoc = process(text);
			AnnotationSet organismAnnotationSet  = annotatedDoc.getAnnotations().get("Organism");	
			doc.getAnnotations()
			.addAll(organismAnnotationSet);				
	}
	
}
