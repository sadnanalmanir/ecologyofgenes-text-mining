/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.ecologyofgenes;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.creole.ResourceInstantiationException;
import gate.util.InvalidOffsetException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author sadnana
 */
class AnnotatedXML2CSVExporter {

    private static final Logger log = Logger.getLogger(AnnotatedXML2CSVExporter.class);

    public AnnotatedXML2CSVExporter() throws Exception {

        if (!Gate.isInitialised()) {
            Properties pro = new Properties();
            try {
                //pro.load(new FileInputStream(new File("resources/project.properties")));
                pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //
            // Load GATE.
            //
            String gateHome = pro.getProperty("GATE_HOME");
            log.info("Initializing GATE...");
            // Set GATE HOME directory.
            Gate.setGateHome(new File(gateHome));
            // Set GATE plugins directory.
            Gate.setPluginsHome(new File(gateHome, "plugins"));
            System.out.println(Gate.getPluginsHome());
            // Initialise GATE.
            Gate.init();
            log.info("Initializing GATE... Done.");
        }
    }

    static void export2CSV(String xmlOutputDirName, BufferedWriter annBuffWriter) throws Exception {
        //
        // Create gate document from input text.
        //

        Map<String, String> fileIndex = Utils.initFileIndex(xmlOutputDirName);

        // Monitor which file is currently processed.
        int numberOfFilesToProcess = fileIndex.size();
        int numberOfFileProcessed = 0;
        /*
         String outputFile = "statAnnot.csv";
         BufferedWriter statsBuffWriter = null;
         statsBuffWriter = new BufferedWriter(new FileWriter(outputFile, false));
         statsBuffWriter.write("document" + "\t"
         + "Abstract" + "\t"
         + "Modifier" + "\t");
         */
        //BufferedWriter annBuffWriter = new BufferedWriter(new FileWriter("annotations.csv", false));

        for (String fileName : fileIndex.keySet()) {

            numberOfFileProcessed++;
            log.info("\n=================================================================\n"
                    + "DOCUMENT: " + fileName + "		" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date())
                    + "\t" + numberOfFileProcessed + " of " + numberOfFilesToProcess
                    + "\n=================================================================\n");

            gate.Document doc = Factory.newDocument(new URL(fileIndex.get(fileName)));
            doc.setName(fileName);

            //writeAnnotation2CSV(doc, "Abstract", "ann", null, annBuffWriter);
            //writeAnnotation2CSV(doc, "Modifier", "ann", "id", annBuffWriter); 
            
            writeAnnotation2CSV(doc, "Export", "ann", null, annBuffWriter);
            //writeAnnotation2CSV(doc, "Coo_Export_1_sent", "ann", null, annBuffWriter);
            Factory.deleteResource(doc);
        }
        //annBuffWriter.close();

    }

    /**
     *
     * @param doc document
     * @param type annotation types
     * @param annotationOrRelation
     * @param feature feature to print
     * @param object print annotation into table pf annotations (annotation.csv)
     */
    private static void writeAnnotation2CSV(Document doc, String type, String annotationOrRelation, String feature, BufferedWriter textBuffWriter) throws IOException {

        Set<String> surfaceStrings = new HashSet<String>();
        Set<String> featureValues = new HashSet<String>();

        //Integer[] stats = { 0, 0, 0 };
        // IF GATE ANNOTATION
        if (annotationOrRelation.equalsIgnoreCase("ann")) {
            Set<Annotation> annMentions = doc.getAnnotations().get(type);
            //stats[0] = annMentions.size();
            //System.out.println("Annotation of type == " + type + " is :" + annMentions.size());
            for (Annotation ann : annMentions) {
                try {
                    String annString = doc.getContent().getContent(ann.getStartNode().getOffset(), ann.getEndNode().getOffset()).toString().replace("\n", " ").trim();
                    //System.out.println("annString : "+ annString);
                    if (annString != null && !annString.equals("") && !annString.equals(" ")) {
                        surfaceStrings.add(annString);
                    }
                    if (feature != null) {
                        String featureValue = (String) ann.getFeatures().get(feature);
                        if (featureValue != null && !featureValue.equals("") && !featureValue.equals(" ")) {
                            featureValues.add(featureValue);
                        }
                    } else {
                        featureValues.add(annString);
                    }
                    //System.out.println("features : " + featureValues.toString());
                    if (textBuffWriter != null) {
                        //System.out.println("not null");
                        //textBuffWriter.write(doc.getName() + "\t" + ann.getType() + "\t" + annString + "\t" + (String) ann.getFeatures().get(feature) + "\n");
                        textBuffWriter.write(doc.getName() + "\t" + annString + "\t" + (String) ann.getFeatures().get("Modifier") + "\t" + (String) ann.getFeatures().get("Object") + "\t" + (String) ann.getFeatures().get("Protein") + "\n");
                    }
                } catch (InvalidOffsetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {

        String xmlOutputDirName = "/home/sadnana/work/cri/development/projects/foodweb/Nathalie/EcolAnn/annotatedXMLOutput";
        BufferedWriter annBuffWriter = new BufferedWriter(new FileWriter("annotations.csv", false));
        AnnotatedXML2CSVExporter annotatedXML2CSVExporter = new AnnotatedXML2CSVExporter();
        AnnotatedXML2CSVExporter.export2CSV(xmlOutputDirName, annBuffWriter);

    }

}
