/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.ecologyofgenes;

import gate.Corpus;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import org.apache.log4j.Logger;

/**
 *
 * @author sadnana
 */
class Pipeline implements Serializable {

    private static final Logger log = Logger.getLogger(Pipeline.class);

    private SerialAnalyserController annieController;

    public Pipeline(String gateHome) throws GateException {
        if (!Gate.isInitialised()) {
            log.info("Initializing GATE...");

            // Set GATE HOME directory.
            Gate.setGateHome(new File(gateHome));
            // Set GATE plugins directory.
            Gate.setPluginsHome(new File(gateHome, "plugins"));
            System.out.println(Gate.getPluginsHome());
            // Initialise GATE.
            Gate.init();
            log.info("Initializing GATE... Done.");
        }
    }

    public void init(boolean runDocumentResetter,
            boolean runTokenizer,
            boolean runSentenceSplitter,
            boolean runPosTagger,
            boolean runCooccurrenceExtractor
    ) throws GateException, URISyntaxException, FileNotFoundException, IOException {

        System.out.println("Pipeline started....");

        // Load ANNIE plugin.
        File pluginsHome = Gate.getPluginsHome();
        Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "ANNIE").toURI().toURL());

        // Create a serial analyser controller to run ANNIE with.
        annieController = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController",
                Factory.newFeatureMap(), Factory.newFeatureMap(), "ANNIE_" + Gate.genSym());

        // ANNIE Document Resetter
        if (runDocumentResetter) {
            // Add tokenizer.
            FeatureMap prParams = Factory.newFeatureMap();
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", prParams);
            annieController.add(pr);
            log.info("document resetter added.");
        }

        if (runTokenizer) {
            // Add tokenizer.
            FeatureMap tokParams = Factory.newFeatureMap();
            ProcessingResource tokPr = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser", tokParams);
            annieController.add(tokPr);
            log.info("tokenizer added");
        }

        if (runSentenceSplitter) {
            // Add sentence splitter.
            FeatureMap senParams = Factory.newFeatureMap();
            ProcessingResource senPr = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter", senParams);
            annieController.add(senPr);
            log.info("sentence splitter added");
        }

        if (runPosTagger) {
            // Add sentence splitter.
            FeatureMap posParams = Factory.newFeatureMap();
            ProcessingResource posPr = (ProcessingResource) Factory.createResource("gate.creole.POSTagger", posParams);
            annieController.add(posPr);
            log.info("POSTagger added");
        }                                        

        {
            log.info("Ecology gazetteer adding...");
            FeatureMap owParams = Factory.newFeatureMap();
            owParams.put("caseSensitive", false);
            owParams.put("encoding", "UTF-8");
            owParams.put("gazetteerFeatureSeparator", "@");
            owParams.put("listsURL", this.getClass().getClassLoader().getResource("gate/gazetteers/ecology.def").toURI().toURL());
            owParams.put("longestMatchOnly", true);
            ProcessingResource owPr = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer", owParams);
            annieController.add(owPr);
            log.info("Ecology gazetteer added");
        }

        // A Biomedical Named Entity Recognizer : tagger for genes, proteins
        {
            log.info("Abner tagger adding...");
            Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Tagger_Abner").toURI().toURL());
            ProcessingResource abnerPr = (ProcessingResource) Factory.createResource("gate.abner.AbnerTagger", Factory.newFeatureMap());
            annieController.add(abnerPr);
            log.info("Abner tagger added");
        }
        
        
        {
            log.info("Lookup transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/LookupTransducer.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("Lookup transducer added");
        }

        {
            log.info("Modifier transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/modifier.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("Modifier transducer added");
        }
        
        {
            log.info("object transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/object.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("object transducer added");
        }
        
        {
            log.info("Abstract transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/titleabstract.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("Abstract transducer added");
        }
        
        {
            log.info("combine transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/combine.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("combine transducer added");
        }
        
        if (runCooccurrenceExtractor) {
            log.info("cooccurrence transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/CooccurrenceMatcher.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("cooccurrence transducer added");
        }
        
    }

    public void setCorpus(Corpus corpus) {
        annieController.setCorpus(corpus);
    }

    public void execute(gate.Document doc) throws GateException {
        log.info("Running Pipeline...");
        gate.Corpus gateCorpus = Factory.newCorpus("corpus" + doc.getName());
        gateCorpus.add(doc);
        this.setCorpus(gateCorpus);
        annieController.execute();

        Factory.deleteResource(gateCorpus);

        log.info("...Pipeline complete");
    }

}
